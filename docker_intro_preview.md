ubuntu@192.168.122.195: Permission denied (publickey).
Traceback (most recent call last):
  File "/usr/bin/uvt-kvm", line 35, in <module>
    uvtool.libvirt.kvm.main_cli_wrapper(sys.argv[1:])
  File "/usr/lib/python2.7/dist-packages/uvtool/libvirt/kvm.py", line 861, in main_cli_wrapper
    main(*args, **kwargs)
  File "/usr/lib/python2.7/dist-packages/uvtool/libvirt/kvm.py", line 856, in main
    args.func(parser, args)
  File "/usr/lib/python2.7/dist-packages/uvtool/libvirt/kvm.py", line 782, in main_wait
    main_wait_remote(parser, args)
  File "/usr/lib/python2.7/dist-packages/uvtool/libvirt/kvm.py", line 745, in main_wait_remote
    insecure=args.insecure,
  File "/usr/lib/python2.7/dist-packages/uvtool/libvirt/kvm.py", line 614, in ssh
    ssh_call, preexec_fn=subprocess_setup, close_fds=True, stdin=stdin
  File "/usr/lib/python2.7/subprocess.py", line 190, in check_call
    raise CalledProcessError(retcode, cmd)
subprocess.CalledProcessError: Command '[u'ssh', u'-o', u'UserKnownHostsFile=/tmp/uvt-kvm.known_hoststmpsJ4juv', u'-l', u'ubuntu', u'192.168.122.195', u'env', u'UVTOOL_WAIT_INTERVAL=8.0', u'UVTOOL_WAIT_TIMEOUT=120.0', u'sh', u'-']' returned non-zero exit status 255
Domain snapshot initial created
# Rails dev in Docker

## Requirenmets

* OS: Ubuntu
* version: bionic

## Resources

* [docker reference documentation](https://docs.docker.com/reference/)

Prepareing docker environment.

```sh
sudo apt-get --yes update
sudo apt-get --yes upgrade
sudo apt-get --yes install apt-transport-https ca-certificates curl \
                            gnupg-agent software-properties-common \
                            postgresql-client redis-tools git
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
sudo apt-get --yes update
sudo apt-get --yes install docker-ce docker-ce-cli containerd.io docker-compose
sudo adduser $USER docker
```

```output
# 192.168.122.195:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3
# 192.168.122.195:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3
# 192.168.122.195:22 SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3
Hit:1 http://archive.ubuntu.com/ubuntu bionic InRelease
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
Get:3 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-backports InRelease [74.6 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [8,570 kB]
Get:6 http://security.ubuntu.com/ubuntu bionic-security/main amd64 Packages [636 kB]
Get:7 http://archive.ubuntu.com/ubuntu bionic/universe Translation-en [4,941 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [151 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic/multiverse Translation-en [108 kB]
Get:10 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [868 kB]
Get:11 http://archive.ubuntu.com/ubuntu bionic-updates/main Translation-en [301 kB]
Get:12 http://security.ubuntu.com/ubuntu bionic-security/main Translation-en [208 kB]
Get:13 http://security.ubuntu.com/ubuntu bionic-security/restricted amd64 Packages [21.2 kB]
Get:14 http://security.ubuntu.com/ubuntu bionic-security/restricted Translation-en [5,984 B]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [32.7 kB]
Get:16 http://security.ubuntu.com/ubuntu bionic-security/universe amd64 Packages [645 kB]
Get:17 http://archive.ubuntu.com/ubuntu bionic-updates/restricted Translation-en [8,440 B]
Get:18 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 Packages [1,052 kB]
Get:19 http://archive.ubuntu.com/ubuntu bionic-updates/universe Translation-en [326 kB]
Get:20 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse amd64 Packages [9,700 B]
Get:21 http://archive.ubuntu.com/ubuntu bionic-updates/multiverse Translation-en [4,576 B]
Get:22 http://archive.ubuntu.com/ubuntu bionic-backports/main amd64 Packages [2,512 B]
Get:23 http://archive.ubuntu.com/ubuntu bionic-backports/main Translation-en [1,644 B]
Get:24 http://archive.ubuntu.com/ubuntu bionic-backports/universe amd64 Packages [4,028 B]
Get:25 http://archive.ubuntu.com/ubuntu bionic-backports/universe Translation-en [1,900 B]
Get:26 http://security.ubuntu.com/ubuntu bionic-security/universe Translation-en [217 kB]
Get:27 http://security.ubuntu.com/ubuntu bionic-security/multiverse amd64 Packages [6,340 B]
Get:28 http://security.ubuntu.com/ubuntu bionic-security/multiverse Translation-en [2,640 B]
Fetched 18.4 MB in 5s (4,080 kB/s)
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Calculating upgrade...
The following package was automatically installed and is no longer required:
  grub-pc-bin
Use 'sudo apt autoremove' to remove it.
The following packages have been kept back:
  linux-headers-generic linux-headers-virtual linux-image-virtual
  linux-virtual
The following packages will be upgraded:
  apport base-files bsdutils dmidecode fdisk libblkid1 libdrm-common libdrm2
  libfdisk1 libmount1 libnss-systemd libpam-systemd libsmartcols1 libsystemd0
  libudev1 libuuid1 libxml2 mdadm mount python3-apport python3-distupgrade
  python3-problem-report sudo systemd systemd-sysv ubuntu-minimal
  ubuntu-release-upgrader-core ubuntu-server ubuntu-standard udev util-linux
  uuid-runtime
32 upgraded, 0 newly installed, 0 to remove and 4 not upgraded.
Need to get 8,252 kB of archives.
After this operation, 56.3 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 base-files amd64 10.1ubuntu2.8 [59.9 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 bsdutils amd64 1:2.31.1-0.4ubuntu3.5 [60.2 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libuuid1 amd64 2.31.1-0.4ubuntu3.5 [20.1 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libblkid1 amd64 2.31.1-0.4ubuntu3.5 [124 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libfdisk1 amd64 2.31.1-0.4ubuntu3.5 [164 kB]
Get:6 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libmount1 amd64 2.31.1-0.4ubuntu3.5 [136 kB]
Get:7 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libsmartcols1 amd64 2.31.1-0.4ubuntu3.5 [83.7 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 fdisk amd64 2.31.1-0.4ubuntu3.5 [108 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 util-linux amd64 2.31.1-0.4ubuntu3.5 [903 kB]
Get:10 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libnss-systemd amd64 237-3ubuntu10.39 [104 kB]
Get:11 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libsystemd0 amd64 237-3ubuntu10.39 [206 kB]
Get:12 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libpam-systemd amd64 237-3ubuntu10.39 [107 kB]
Get:13 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 systemd amd64 237-3ubuntu10.39 [2,912 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 udev amd64 237-3ubuntu10.39 [1,102 kB]
Get:15 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libudev1 amd64 237-3ubuntu10.39 [56.1 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 systemd-sysv amd64 237-3ubuntu10.39 [13.9 kB]
Get:17 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 mount amd64 2.31.1-0.4ubuntu3.5 [107 kB]
Get:18 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 uuid-runtime amd64 2.31.1-0.4ubuntu3.5 [34.8 kB]
Get:19 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libxml2 amd64 2.9.4+dfsg1-6.1ubuntu1.3 [663 kB]
Get:20 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 sudo amd64 1.8.21p2-3ubuntu1.2 [427 kB]
Get:21 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 ubuntu-minimal amd64 1.417.4 [2,704 B]
Get:22 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 dmidecode amd64 3.1-1ubuntu0.1 [50.9 kB]
Get:23 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libdrm-common all 2.4.99-1ubuntu1~18.04.2 [5,328 B]
Get:24 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libdrm2 amd64 2.4.99-1ubuntu1~18.04.2 [31.7 kB]
Get:25 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 ubuntu-release-upgrader-core all 1:18.04.37 [25.4 kB]
Get:26 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python3-distupgrade all 1:18.04.37 [106 kB]
Get:27 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 ubuntu-standard amd64 1.417.4 [2,752 B]
Get:28 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python3-problem-report all 2.20.9-0ubuntu7.11 [10.6 kB]
Get:29 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python3-apport all 2.20.9-0ubuntu7.11 [81.8 kB]
Get:30 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 apport all 2.20.9-0ubuntu7.11 [124 kB]
Get:31 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 mdadm amd64 4.1~rc1-3~ubuntu18.04.4 [416 kB]
Get:32 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 ubuntu-server amd64 1.417.4 [2,608 B]
debconf: unable to initialize frontend: Dialog
debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (This frontend requires a controlling tty.)
debconf: falling back to frontend: Teletype
dpkg-preconfigure: unable to re-open stdin: 
Fetched 8,252 kB in 1s (7,948 kB/s)
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../base-files_10.1ubuntu2.8_amd64.deb ...
Warning: Stopping motd-news.service, but it can still be activated by:
  motd-news.timer
Unpacking base-files (10.1ubuntu2.8) over (10.1ubuntu2.7) ...
Setting up base-files (10.1ubuntu2.8) ...
Installing new version of config file /etc/issue ...
Installing new version of config file /etc/issue.net ...
Installing new version of config file /etc/lsb-release ...
motd-news.service is a disabled or a static unit, not starting it.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../bsdutils_1%3a2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking bsdutils (1:2.31.1-0.4ubuntu3.5) over (1:2.31.1-0.4ubuntu3.4) ...
Setting up bsdutils (1:2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libuuid1_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking libuuid1:amd64 (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up libuuid1:amd64 (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libblkid1_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking libblkid1:amd64 (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up libblkid1:amd64 (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libfdisk1_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking libfdisk1:amd64 (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up libfdisk1:amd64 (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libmount1_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking libmount1:amd64 (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up libmount1:amd64 (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libsmartcols1_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking libsmartcols1:amd64 (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up libsmartcols1:amd64 (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../fdisk_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking fdisk (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up fdisk (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../util-linux_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking util-linux (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Setting up util-linux (2.31.1-0.4ubuntu3.5) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libnss-systemd_237-3ubuntu10.39_amd64.deb ...
Unpacking libnss-systemd:amd64 (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Preparing to unpack .../libsystemd0_237-3ubuntu10.39_amd64.deb ...
Unpacking libsystemd0:amd64 (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Setting up libsystemd0:amd64 (237-3ubuntu10.39) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../libpam-systemd_237-3ubuntu10.39_amd64.deb ...
Unpacking libpam-systemd:amd64 (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Preparing to unpack .../systemd_237-3ubuntu10.39_amd64.deb ...
Unpacking systemd (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Preparing to unpack .../udev_237-3ubuntu10.39_amd64.deb ...
Unpacking udev (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Preparing to unpack .../libudev1_237-3ubuntu10.39_amd64.deb ...
Unpacking libudev1:amd64 (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Setting up libudev1:amd64 (237-3ubuntu10.39) ...
Setting up systemd (237-3ubuntu10.39) ...
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60037 files and directories currently installed.)
Preparing to unpack .../00-systemd-sysv_237-3ubuntu10.39_amd64.deb ...
Unpacking systemd-sysv (237-3ubuntu10.39) over (237-3ubuntu10.33) ...
Preparing to unpack .../01-mount_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking mount (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Preparing to unpack .../02-uuid-runtime_2.31.1-0.4ubuntu3.5_amd64.deb ...
Unpacking uuid-runtime (2.31.1-0.4ubuntu3.5) over (2.31.1-0.4ubuntu3.4) ...
Preparing to unpack .../03-libxml2_2.9.4+dfsg1-6.1ubuntu1.3_amd64.deb ...
Unpacking libxml2:amd64 (2.9.4+dfsg1-6.1ubuntu1.3) over (2.9.4+dfsg1-6.1ubuntu1.2) ...
Preparing to unpack .../04-sudo_1.8.21p2-3ubuntu1.2_amd64.deb ...
Unpacking sudo (1.8.21p2-3ubuntu1.2) over (1.8.21p2-3ubuntu1.1) ...
Preparing to unpack .../05-ubuntu-minimal_1.417.4_amd64.deb ...
Unpacking ubuntu-minimal (1.417.4) over (1.417.3) ...
Preparing to unpack .../06-dmidecode_3.1-1ubuntu0.1_amd64.deb ...
Unpacking dmidecode (3.1-1ubuntu0.1) over (3.1-1) ...
Preparing to unpack .../07-libdrm-common_2.4.99-1ubuntu1~18.04.2_all.deb ...
Unpacking libdrm-common (2.4.99-1ubuntu1~18.04.2) over (2.4.99-1ubuntu1~18.04.1) ...
Preparing to unpack .../08-libdrm2_2.4.99-1ubuntu1~18.04.2_amd64.deb ...
Unpacking libdrm2:amd64 (2.4.99-1ubuntu1~18.04.2) over (2.4.99-1ubuntu1~18.04.1) ...
Preparing to unpack .../09-ubuntu-release-upgrader-core_1%3a18.04.37_all.deb ...
Unpacking ubuntu-release-upgrader-core (1:18.04.37) over (1:18.04.36) ...
Preparing to unpack .../10-python3-distupgrade_1%3a18.04.37_all.deb ...
Unpacking python3-distupgrade (1:18.04.37) over (1:18.04.36) ...
Preparing to unpack .../11-ubuntu-standard_1.417.4_amd64.deb ...
Unpacking ubuntu-standard (1.417.4) over (1.417.3) ...
Preparing to unpack .../12-python3-problem-report_2.20.9-0ubuntu7.11_all.deb ...
Unpacking python3-problem-report (2.20.9-0ubuntu7.11) over (2.20.9-0ubuntu7.9) ...
Preparing to unpack .../13-python3-apport_2.20.9-0ubuntu7.11_all.deb ...
Unpacking python3-apport (2.20.9-0ubuntu7.11) over (2.20.9-0ubuntu7.9) ...
Preparing to unpack .../14-apport_2.20.9-0ubuntu7.11_all.deb ...
Unpacking apport (2.20.9-0ubuntu7.11) over (2.20.9-0ubuntu7.9) ...
Preparing to unpack .../15-mdadm_4.1~rc1-3~ubuntu18.04.4_amd64.deb ...
Unpacking mdadm (4.1~rc1-3~ubuntu18.04.4) over (4.1~rc1-3~ubuntu18.04.2) ...
Preparing to unpack .../16-ubuntu-server_1.417.4_amd64.deb ...
Unpacking ubuntu-server (1.417.4) over (1.417.3) ...
Setting up libnss-systemd:amd64 (237-3ubuntu10.39) ...
Setting up sudo (1.8.21p2-3ubuntu1.2) ...
Setting up dmidecode (3.1-1ubuntu0.1) ...
Setting up systemd-sysv (237-3ubuntu10.39) ...
Setting up mount (2.31.1-0.4ubuntu3.5) ...
Setting up libxml2:amd64 (2.9.4+dfsg1-6.1ubuntu1.3) ...
Setting up uuid-runtime (2.31.1-0.4ubuntu3.5) ...
Setting up libdrm-common (2.4.99-1ubuntu1~18.04.2) ...
Setting up python3-problem-report (2.20.9-0ubuntu7.11) ...
Setting up udev (237-3ubuntu10.39) ...
update-initramfs: deferring update (trigger activated)
Setting up ubuntu-minimal (1.417.4) ...
Setting up python3-distupgrade (1:18.04.37) ...
Setting up libpam-systemd:amd64 (237-3ubuntu10.39) ...
debconf: unable to initialize frontend: Dialog
debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
debconf: falling back to frontend: Readline
Setting up ubuntu-release-upgrader-core (1:18.04.37) ...
Setting up python3-apport (2.20.9-0ubuntu7.11) ...
Setting up mdadm (4.1~rc1-3~ubuntu18.04.4) ...
debconf: unable to initialize frontend: Dialog
debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
debconf: falling back to frontend: Readline
update-initramfs: deferring update (trigger activated)
Sourcing file `/etc/default/grub'
Sourcing file `/etc/default/grub.d/50-cloudimg-settings.cfg'
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-4.15.0-76-generic
Found initrd image: /boot/initrd.img-4.15.0-76-generic
File descriptor 3 (pipe:[27840]) leaked on lvs invocation. Parent PID 3725: /bin/sh
done
update-rc.d: warning: start and stop actions are no longer supported; falling back to defaults
Setting up libdrm2:amd64 (2.4.99-1ubuntu1~18.04.2) ...
Setting up apport (2.20.9-0ubuntu7.11) ...
apport-autoreport.service is a disabled or a static unit, not starting it.
Setting up ubuntu-standard (1.417.4) ...
Setting up ubuntu-server (1.417.4) ...
Processing triggers for systemd (237-3ubuntu10.39) ...
Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
Processing triggers for dbus (1.12.2-1ubuntu1.1) ...
Processing triggers for mime-support (3.60ubuntu1) ...
Processing triggers for ureadahead (0.100.0-21) ...
Processing triggers for install-info (6.5.0.dfsg.1-2) ...
Processing triggers for plymouth-theme-ubuntu-text (0.9.3-1ubuntu7.18.04.2) ...
update-initramfs: deferring update (trigger activated)
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Processing triggers for initramfs-tools (0.130ubuntu3.9) ...
update-initramfs: Generating /boot/initrd.img-4.15.0-76-generic
Reading package lists...
Building dependency tree...
Reading state information...
ca-certificates is already the newest version (20180409).
ca-certificates set to manually installed.
curl is already the newest version (7.58.0-2ubuntu3.8).
curl set to manually installed.
git is already the newest version (1:2.17.1-1ubuntu0.5).
git set to manually installed.
software-properties-common is already the newest version (0.96.24.32.12).
software-properties-common set to manually installed.
The following package was automatically installed and is no longer required:
  grub-pc-bin
Use 'sudo apt autoremove' to remove it.
Suggested packages:
  postgresql-10 postgresql-doc-10 ruby-redis
The following NEW packages will be installed:
  apt-transport-https gnupg-agent libjemalloc1 libpq5 postgresql-client
  postgresql-client-10 postgresql-client-common redis-tools
0 upgraded, 8 newly installed, 0 to remove and 4 not upgraded.
Need to get 1,683 kB of archives.
After this operation, 6,860 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 apt-transport-https all 1.6.12 [1,692 B]
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 gnupg-agent all 2.2.4-1ubuntu1.2 [4,880 B]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libpq5 amd64 10.10-0ubuntu0.18.04.1 [108 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 postgresql-client-common all 190ubuntu0.1 [29.6 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 postgresql-client-10 amd64 10.10-0ubuntu0.18.04.1 [935 kB]
Get:6 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 postgresql-client all 10+190ubuntu0.1 [5,896 B]
Get:7 http://archive.ubuntu.com/ubuntu bionic/universe amd64 libjemalloc1 amd64 3.6.0-11 [82.4 kB]
Get:8 http://archive.ubuntu.com/ubuntu bionic-updates/universe amd64 redis-tools amd64 5:4.0.9-1ubuntu0.2 [516 kB]
debconf: unable to initialize frontend: Dialog
debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (This frontend requires a controlling tty.)
debconf: falling back to frontend: Teletype
dpkg-preconfigure: unable to re-open stdin: 
Fetched 1,683 kB in 0s (3,441 kB/s)
Selecting previously unselected package apt-transport-https.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60043 files and directories currently installed.)
Preparing to unpack .../0-apt-transport-https_1.6.12_all.deb ...
Unpacking apt-transport-https (1.6.12) ...
Selecting previously unselected package gnupg-agent.
Preparing to unpack .../1-gnupg-agent_2.2.4-1ubuntu1.2_all.deb ...
Unpacking gnupg-agent (2.2.4-1ubuntu1.2) ...
Selecting previously unselected package libpq5:amd64.
Preparing to unpack .../2-libpq5_10.10-0ubuntu0.18.04.1_amd64.deb ...
Unpacking libpq5:amd64 (10.10-0ubuntu0.18.04.1) ...
Selecting previously unselected package postgresql-client-common.
Preparing to unpack .../3-postgresql-client-common_190ubuntu0.1_all.deb ...
Unpacking postgresql-client-common (190ubuntu0.1) ...
Selecting previously unselected package postgresql-client-10.
Preparing to unpack .../4-postgresql-client-10_10.10-0ubuntu0.18.04.1_amd64.deb ...
Unpacking postgresql-client-10 (10.10-0ubuntu0.18.04.1) ...
Selecting previously unselected package postgresql-client.
Preparing to unpack .../5-postgresql-client_10+190ubuntu0.1_all.deb ...
Unpacking postgresql-client (10+190ubuntu0.1) ...
Selecting previously unselected package libjemalloc1.
Preparing to unpack .../6-libjemalloc1_3.6.0-11_amd64.deb ...
Unpacking libjemalloc1 (3.6.0-11) ...
Selecting previously unselected package redis-tools.
Preparing to unpack .../7-redis-tools_5%3a4.0.9-1ubuntu0.2_amd64.deb ...
Unpacking redis-tools (5:4.0.9-1ubuntu0.2) ...
Setting up apt-transport-https (1.6.12) ...
Setting up libjemalloc1 (3.6.0-11) ...
Setting up libpq5:amd64 (10.10-0ubuntu0.18.04.1) ...
Setting up postgresql-client-common (190ubuntu0.1) ...
Setting up gnupg-agent (2.2.4-1ubuntu1.2) ...
Setting up postgresql-client-10 (10.10-0ubuntu0.18.04.1) ...
update-alternatives: using /usr/share/postgresql/10/man/man1/psql.1.gz to provide /usr/share/man/man1/psql.1.gz (psql.1.gz) in auto mode
Setting up postgresql-client (10+190ubuntu0.1) ...
Setting up redis-tools (5:4.0.9-1ubuntu0.2) ...
Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Warning: apt-key output should not be parsed (stdout is not a terminal)
OK
Get:1 https://download.docker.com/linux/ubuntu bionic InRelease [64.4 kB]
Hit:2 http://archive.ubuntu.com/ubuntu bionic InRelease
Get:3 http://security.ubuntu.com/ubuntu bionic-security InRelease [88.7 kB]
Hit:4 http://archive.ubuntu.com/ubuntu bionic-updates InRelease
Hit:5 http://archive.ubuntu.com/ubuntu bionic-backports InRelease
Get:6 https://download.docker.com/linux/ubuntu bionic/stable amd64 Packages [10.3 kB]
Fetched 163 kB in 1s (266 kB/s)
Reading package lists...
Hit:1 https://download.docker.com/linux/ubuntu bionic InRelease
Hit:2 http://archive.ubuntu.com/ubuntu bionic InRelease
Hit:3 http://archive.ubuntu.com/ubuntu bionic-updates InRelease
Hit:4 http://archive.ubuntu.com/ubuntu bionic-backports InRelease
Hit:5 http://security.ubuntu.com/ubuntu bionic-security InRelease
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following package was automatically installed and is no longer required:
  grub-pc-bin
Use 'sudo apt autoremove' to remove it.
The following additional packages will be installed:
  aufs-tools cgroupfs-mount golang-docker-credential-helpers libltdl7
  libpython-stdlib libpython2.7-minimal libpython2.7-stdlib libsecret-1-0
  libsecret-common pigz python python-asn1crypto
  python-backports.ssl-match-hostname python-cached-property python-certifi
  python-cffi-backend python-chardet python-cryptography python-docker
  python-dockerpty python-dockerpycreds python-docopt python-enum34
  python-funcsigs python-functools32 python-idna python-ipaddress
  python-jsonschema python-minimal python-mock python-openssl python-pbr
  python-pkg-resources python-requests python-six python-texttable
  python-urllib3 python-websocket python-yaml python2.7 python2.7-minimal
Suggested packages:
  python-doc python-tk python-cryptography-doc python-cryptography-vectors
  python-enum34-doc python-funcsigs-doc python-mock-doc python-openssl-doc
  python-openssl-dbg python-setuptools python-socks python-ntlm python2.7-doc
  binutils binfmt-support
Recommended packages:
  docker.io
The following NEW packages will be installed:
  aufs-tools cgroupfs-mount containerd.io docker-ce docker-ce-cli
  docker-compose golang-docker-credential-helpers libltdl7 libpython-stdlib
  libpython2.7-minimal libpython2.7-stdlib libsecret-1-0 libsecret-common pigz
  python python-asn1crypto python-backports.ssl-match-hostname
  python-cached-property python-certifi python-cffi-backend python-chardet
  python-cryptography python-docker python-dockerpty python-dockerpycreds
  python-docopt python-enum34 python-funcsigs python-functools32 python-idna
  python-ipaddress python-jsonschema python-minimal python-mock python-openssl
  python-pbr python-pkg-resources python-requests python-six python-texttable
  python-urllib3 python-websocket python-yaml python2.7 python2.7-minimal
0 upgraded, 45 newly installed, 0 to remove and 4 not upgraded.
Need to get 91.7 MB of archives.
After this operation, 411 MB of additional disk space will be used.
Get:1 https://download.docker.com/linux/ubuntu bionic/stable amd64 containerd.io amd64 1.2.10-3 [20.0 MB]
Get:2 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libpython2.7-minimal amd64 2.7.17-1~18.04 [335 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python2.7-minimal amd64 2.7.17-1~18.04 [1,294 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-minimal amd64 2.7.15~rc1-1 [28.1 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 libpython2.7-stdlib amd64 2.7.17-1~18.04 [1,915 kB]
Get:6 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python2.7 amd64 2.7.17-1~18.04 [248 kB]
Get:7 http://archive.ubuntu.com/ubuntu bionic/main amd64 libpython-stdlib amd64 2.7.15~rc1-1 [7,620 B]
Get:8 http://archive.ubuntu.com/ubuntu bionic/main amd64 python amd64 2.7.15~rc1-1 [140 kB]
Get:9 http://archive.ubuntu.com/ubuntu bionic/universe amd64 pigz amd64 2.4-1 [57.4 kB]
Get:10 http://archive.ubuntu.com/ubuntu bionic/universe amd64 aufs-tools amd64 1:4.9+20170918-1ubuntu1 [104 kB]
Get:11 http://archive.ubuntu.com/ubuntu bionic/universe amd64 cgroupfs-mount all 1.4 [6,320 B]
Get:12 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-backports.ssl-match-hostname all 3.5.0.1-1 [7,024 B]
Get:13 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-pkg-resources all 39.0.1-2 [128 kB]
Get:14 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-cached-property all 1.3.1-1 [7,568 B]
Get:15 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-six all 1.11.0-2 [11.3 kB]
Get:16 http://archive.ubuntu.com/ubuntu bionic/main amd64 libsecret-common all 0.18.6-1 [4,452 B]
Get:17 http://archive.ubuntu.com/ubuntu bionic/main amd64 libsecret-1-0 amd64 0.18.6-1 [94.6 kB]
Get:18 http://archive.ubuntu.com/ubuntu bionic/universe amd64 golang-docker-credential-helpers amd64 0.5.0-2 [444 kB]
Get:19 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-dockerpycreds all 0.2.1-1 [4,138 B]
Get:20 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-certifi all 2018.1.18-2 [144 kB]
Get:21 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-chardet all 3.0.4-1 [80.3 kB]
Get:22 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-idna all 2.6-1 [32.4 kB]
Get:23 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python-urllib3 all 1.22-1ubuntu0.18.04.1 [85.9 kB]
Get:24 https://download.docker.com/linux/ubuntu bionic/stable amd64 docker-ce-cli amd64 5:19.03.6~3-0~ubuntu-bionic [42.5 MB]
Get:25 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python-requests all 2.18.4-2ubuntu0.1 [58.5 kB]
Get:26 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-websocket all 0.44.0-0ubuntu2 [30.7 kB]
Get:27 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-ipaddress all 1.0.17-1 [18.2 kB]
Get:28 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-docker all 2.5.1-1 [69.0 kB]
Get:29 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-dockerpty all 0.4.1-1 [10.8 kB]
Get:30 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-docopt all 0.6.2-1build1 [25.6 kB]
Get:31 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-enum34 all 1.1.6-2 [34.8 kB]
Get:32 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-functools32 all 3.2.3.2-3 [10.8 kB]
Get:33 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-funcsigs all 1.0.2-4 [13.5 kB]
Get:34 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-pbr all 3.1.1-3ubuntu3 [53.7 kB]
Get:35 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-mock all 2.0.0-3 [47.4 kB]
Get:36 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-jsonschema all 2.6.0-2 [31.5 kB]
Get:37 http://archive.ubuntu.com/ubuntu bionic/universe amd64 python-texttable all 0.9.1-1 [8,160 B]
Get:38 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-yaml amd64 3.12-1build2 [115 kB]
Get:39 http://archive.ubuntu.com/ubuntu bionic/universe amd64 docker-compose all 1.17.1-2 [76.3 kB]
Get:40 http://archive.ubuntu.com/ubuntu bionic/main amd64 libltdl7 amd64 2.4.6-2 [38.8 kB]
Get:41 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-asn1crypto all 0.24.0-1 [72.7 kB]
Get:42 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-cffi-backend amd64 1.11.5-1 [63.4 kB]
Get:43 http://archive.ubuntu.com/ubuntu bionic-updates/main amd64 python-cryptography amd64 2.1.4-1ubuntu1.3 [221 kB]
Get:44 http://archive.ubuntu.com/ubuntu bionic/main amd64 python-openssl all 17.5.0-1ubuntu1 [41.3 kB]
Get:45 https://download.docker.com/linux/ubuntu bionic/stable amd64 docker-ce amd64 5:19.03.6~3-0~ubuntu-bionic [22.9 MB]
debconf: unable to initialize frontend: Dialog
debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (This frontend requires a controlling tty.)
debconf: falling back to frontend: Teletype
dpkg-preconfigure: unable to re-open stdin: 
Fetched 91.7 MB in 2s (39.9 MB/s)
Selecting previously unselected package libpython2.7-minimal:amd64.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 60341 files and directories currently installed.)
Preparing to unpack .../0-libpython2.7-minimal_2.7.17-1~18.04_amd64.deb ...
Unpacking libpython2.7-minimal:amd64 (2.7.17-1~18.04) ...
Selecting previously unselected package python2.7-minimal.
Preparing to unpack .../1-python2.7-minimal_2.7.17-1~18.04_amd64.deb ...
Unpacking python2.7-minimal (2.7.17-1~18.04) ...
Selecting previously unselected package python-minimal.
Preparing to unpack .../2-python-minimal_2.7.15~rc1-1_amd64.deb ...
Unpacking python-minimal (2.7.15~rc1-1) ...
Selecting previously unselected package libpython2.7-stdlib:amd64.
Preparing to unpack .../3-libpython2.7-stdlib_2.7.17-1~18.04_amd64.deb ...
Unpacking libpython2.7-stdlib:amd64 (2.7.17-1~18.04) ...
Selecting previously unselected package python2.7.
Preparing to unpack .../4-python2.7_2.7.17-1~18.04_amd64.deb ...
Unpacking python2.7 (2.7.17-1~18.04) ...
Selecting previously unselected package libpython-stdlib:amd64.
Preparing to unpack .../5-libpython-stdlib_2.7.15~rc1-1_amd64.deb ...
Unpacking libpython-stdlib:amd64 (2.7.15~rc1-1) ...
Setting up libpython2.7-minimal:amd64 (2.7.17-1~18.04) ...
Setting up python2.7-minimal (2.7.17-1~18.04) ...
Linking and byte-compiling packages for runtime python2.7...
Setting up python-minimal (2.7.15~rc1-1) ...
Selecting previously unselected package python.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 61089 files and directories currently installed.)
Preparing to unpack .../00-python_2.7.15~rc1-1_amd64.deb ...
Unpacking python (2.7.15~rc1-1) ...
Selecting previously unselected package pigz.
Preparing to unpack .../01-pigz_2.4-1_amd64.deb ...
Unpacking pigz (2.4-1) ...
Selecting previously unselected package aufs-tools.
Preparing to unpack .../02-aufs-tools_1%3a4.9+20170918-1ubuntu1_amd64.deb ...
Unpacking aufs-tools (1:4.9+20170918-1ubuntu1) ...
Selecting previously unselected package cgroupfs-mount.
Preparing to unpack .../03-cgroupfs-mount_1.4_all.deb ...
Unpacking cgroupfs-mount (1.4) ...
Selecting previously unselected package containerd.io.
Preparing to unpack .../04-containerd.io_1.2.10-3_amd64.deb ...
Unpacking containerd.io (1.2.10-3) ...
Selecting previously unselected package docker-ce-cli.
Preparing to unpack .../05-docker-ce-cli_5%3a19.03.6~3-0~ubuntu-bionic_amd64.deb ...
Unpacking docker-ce-cli (5:19.03.6~3-0~ubuntu-bionic) ...
Selecting previously unselected package docker-ce.
Preparing to unpack .../06-docker-ce_5%3a19.03.6~3-0~ubuntu-bionic_amd64.deb ...
Unpacking docker-ce (5:19.03.6~3-0~ubuntu-bionic) ...
Selecting previously unselected package python-backports.ssl-match-hostname.
Preparing to unpack .../07-python-backports.ssl-match-hostname_3.5.0.1-1_all.deb ...
Unpacking python-backports.ssl-match-hostname (3.5.0.1-1) ...
Selecting previously unselected package python-pkg-resources.
Preparing to unpack .../08-python-pkg-resources_39.0.1-2_all.deb ...
Unpacking python-pkg-resources (39.0.1-2) ...
Selecting previously unselected package python-cached-property.
Preparing to unpack .../09-python-cached-property_1.3.1-1_all.deb ...
Unpacking python-cached-property (1.3.1-1) ...
Selecting previously unselected package python-six.
Preparing to unpack .../10-python-six_1.11.0-2_all.deb ...
Unpacking python-six (1.11.0-2) ...
Selecting previously unselected package libsecret-common.
Preparing to unpack .../11-libsecret-common_0.18.6-1_all.deb ...
Unpacking libsecret-common (0.18.6-1) ...
Selecting previously unselected package libsecret-1-0:amd64.
Preparing to unpack .../12-libsecret-1-0_0.18.6-1_amd64.deb ...
Unpacking libsecret-1-0:amd64 (0.18.6-1) ...
Selecting previously unselected package golang-docker-credential-helpers.
Preparing to unpack .../13-golang-docker-credential-helpers_0.5.0-2_amd64.deb ...
Unpacking golang-docker-credential-helpers (0.5.0-2) ...
Selecting previously unselected package python-dockerpycreds.
Preparing to unpack .../14-python-dockerpycreds_0.2.1-1_all.deb ...
Unpacking python-dockerpycreds (0.2.1-1) ...
Selecting previously unselected package python-certifi.
Preparing to unpack .../15-python-certifi_2018.1.18-2_all.deb ...
Unpacking python-certifi (2018.1.18-2) ...
Selecting previously unselected package python-chardet.
Preparing to unpack .../16-python-chardet_3.0.4-1_all.deb ...
Unpacking python-chardet (3.0.4-1) ...
Selecting previously unselected package python-idna.
Preparing to unpack .../17-python-idna_2.6-1_all.deb ...
Unpacking python-idna (2.6-1) ...
Selecting previously unselected package python-urllib3.
Preparing to unpack .../18-python-urllib3_1.22-1ubuntu0.18.04.1_all.deb ...
Unpacking python-urllib3 (1.22-1ubuntu0.18.04.1) ...
Selecting previously unselected package python-requests.
Preparing to unpack .../19-python-requests_2.18.4-2ubuntu0.1_all.deb ...
Unpacking python-requests (2.18.4-2ubuntu0.1) ...
Selecting previously unselected package python-websocket.
Preparing to unpack .../20-python-websocket_0.44.0-0ubuntu2_all.deb ...
Unpacking python-websocket (0.44.0-0ubuntu2) ...
Selecting previously unselected package python-ipaddress.
Preparing to unpack .../21-python-ipaddress_1.0.17-1_all.deb ...
Unpacking python-ipaddress (1.0.17-1) ...
Selecting previously unselected package python-docker.
Preparing to unpack .../22-python-docker_2.5.1-1_all.deb ...
Unpacking python-docker (2.5.1-1) ...
Selecting previously unselected package python-dockerpty.
Preparing to unpack .../23-python-dockerpty_0.4.1-1_all.deb ...
Unpacking python-dockerpty (0.4.1-1) ...
Selecting previously unselected package python-docopt.
Preparing to unpack .../24-python-docopt_0.6.2-1build1_all.deb ...
Unpacking python-docopt (0.6.2-1build1) ...
Selecting previously unselected package python-enum34.
Preparing to unpack .../25-python-enum34_1.1.6-2_all.deb ...
Unpacking python-enum34 (1.1.6-2) ...
Selecting previously unselected package python-functools32.
Preparing to unpack .../26-python-functools32_3.2.3.2-3_all.deb ...
Unpacking python-functools32 (3.2.3.2-3) ...
Selecting previously unselected package python-funcsigs.
Preparing to unpack .../27-python-funcsigs_1.0.2-4_all.deb ...
Unpacking python-funcsigs (1.0.2-4) ...
Selecting previously unselected package python-pbr.
Preparing to unpack .../28-python-pbr_3.1.1-3ubuntu3_all.deb ...
Unpacking python-pbr (3.1.1-3ubuntu3) ...
Selecting previously unselected package python-mock.
Preparing to unpack .../29-python-mock_2.0.0-3_all.deb ...
Unpacking python-mock (2.0.0-3) ...
Selecting previously unselected package python-jsonschema.
Preparing to unpack .../30-python-jsonschema_2.6.0-2_all.deb ...
Unpacking python-jsonschema (2.6.0-2) ...
Selecting previously unselected package python-texttable.
Preparing to unpack .../31-python-texttable_0.9.1-1_all.deb ...
Unpacking python-texttable (0.9.1-1) ...
Selecting previously unselected package python-yaml.
Preparing to unpack .../32-python-yaml_3.12-1build2_amd64.deb ...
Unpacking python-yaml (3.12-1build2) ...
Selecting previously unselected package docker-compose.
Preparing to unpack .../33-docker-compose_1.17.1-2_all.deb ...
Unpacking docker-compose (1.17.1-2) ...
Selecting previously unselected package libltdl7:amd64.
Preparing to unpack .../34-libltdl7_2.4.6-2_amd64.deb ...
Unpacking libltdl7:amd64 (2.4.6-2) ...
Selecting previously unselected package python-asn1crypto.
Preparing to unpack .../35-python-asn1crypto_0.24.0-1_all.deb ...
Unpacking python-asn1crypto (0.24.0-1) ...
Selecting previously unselected package python-cffi-backend.
Preparing to unpack .../36-python-cffi-backend_1.11.5-1_amd64.deb ...
Unpacking python-cffi-backend (1.11.5-1) ...
Selecting previously unselected package python-cryptography.
Preparing to unpack .../37-python-cryptography_2.1.4-1ubuntu1.3_amd64.deb ...
Unpacking python-cryptography (2.1.4-1ubuntu1.3) ...
Selecting previously unselected package python-openssl.
Preparing to unpack .../38-python-openssl_17.5.0-1ubuntu1_all.deb ...
Unpacking python-openssl (17.5.0-1ubuntu1) ...
Setting up aufs-tools (1:4.9+20170918-1ubuntu1) ...
Setting up libsecret-common (0.18.6-1) ...
Setting up containerd.io (1.2.10-3) ...
Created symlink /etc/systemd/system/multi-user.target.wants/containerd.service → /lib/systemd/system/containerd.service.
Setting up cgroupfs-mount (1.4) ...
Setting up libltdl7:amd64 (2.4.6-2) ...
Setting up docker-ce-cli (5:19.03.6~3-0~ubuntu-bionic) ...
Setting up libsecret-1-0:amd64 (0.18.6-1) ...
Setting up pigz (2.4-1) ...
Setting up libpython2.7-stdlib:amd64 (2.7.17-1~18.04) ...
Setting up docker-ce (5:19.03.6~3-0~ubuntu-bionic) ...
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /lib/systemd/system/docker.service.
Created symlink /etc/systemd/system/sockets.target.wants/docker.socket → /lib/systemd/system/docker.socket.
Setting up python2.7 (2.7.17-1~18.04) ...
Setting up libpython-stdlib:amd64 (2.7.15~rc1-1) ...
Setting up golang-docker-credential-helpers (0.5.0-2) ...
Setting up python (2.7.15~rc1-1) ...
Setting up python-idna (2.6-1) ...
Setting up python-texttable (0.9.1-1) ...
Setting up python-functools32 (3.2.3.2-3) ...
Setting up python-yaml (3.12-1build2) ...
Setting up python-asn1crypto (0.24.0-1) ...
Setting up python-certifi (2018.1.18-2) ...
Setting up python-pkg-resources (39.0.1-2) ...
Setting up python-backports.ssl-match-hostname (3.5.0.1-1) ...
Setting up python-cffi-backend (1.11.5-1) ...
Setting up python-six (1.11.0-2) ...
Setting up python-dockerpty (0.4.1-1) ...
Setting up python-pbr (3.1.1-3ubuntu3) ...
update-alternatives: using /usr/bin/python2-pbr to provide /usr/bin/pbr (pbr) in auto mode
Setting up python-enum34 (1.1.6-2) ...
Setting up python-funcsigs (1.0.2-4) ...
Setting up python-docopt (0.6.2-1build1) ...
Setting up python-ipaddress (1.0.17-1) ...
Setting up python-cached-property (1.3.1-1) ...
Setting up python-urllib3 (1.22-1ubuntu0.18.04.1) ...
Setting up python-chardet (3.0.4-1) ...
Setting up python-dockerpycreds (0.2.1-1) ...
Setting up python-mock (2.0.0-3) ...
Setting up python-websocket (0.44.0-0ubuntu2) ...
update-alternatives: using /usr/bin/python2-wsdump to provide /usr/bin/wsdump (wsdump) in auto mode
Setting up python-cryptography (2.1.4-1ubuntu1.3) ...
Setting up python-requests (2.18.4-2ubuntu0.1) ...
Setting up python-jsonschema (2.6.0-2) ...
update-alternatives: using /usr/bin/python2-jsonschema to provide /usr/bin/jsonschema (jsonschema) in auto mode
Setting up python-openssl (17.5.0-1ubuntu1) ...
Setting up python-docker (2.5.1-1) ...
Setting up docker-compose (1.17.1-2) ...
Processing triggers for systemd (237-3ubuntu10.39) ...
Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
Processing triggers for mime-support (3.60ubuntu1) ...
Processing triggers for ureadahead (0.100.0-21) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Adding user `ubuntu' to group `docker' ...
Adding user ubuntu to group docker
Done.
```

Domain snapshot prepared created
```sh
sudo apt-get --yes install elinks
```

```output
Reading package lists...
Building dependency tree...
Reading state information...
The following package was automatically installed and is no longer required:
  grub-pc-bin
Use 'sudo apt autoremove' to remove it.
The following additional packages will be installed:
  elinks-data libfsplib0 liblua5.1-0 libtre5
Suggested packages:
  elinks-doc tre-agrep
The following NEW packages will be installed:
  elinks elinks-data libfsplib0 liblua5.1-0 libtre5
0 upgraded, 5 newly installed, 0 to remove and 4 not upgraded.
Need to get 1,062 kB of archives.
After this operation, 3,777 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu bionic/universe amd64 libfsplib0 amd64 0.11-2 [13.1 kB]
Get:2 http://archive.ubuntu.com/ubuntu bionic/universe amd64 liblua5.1-0 amd64 5.1.5-8.1build2 [100 kB]
Get:3 http://archive.ubuntu.com/ubuntu bionic/universe amd64 libtre5 amd64 0.8.0-6 [47.5 kB]
Get:4 http://archive.ubuntu.com/ubuntu bionic/universe amd64 elinks-data all 0.12~pre6-13 [371 kB]
Get:5 http://archive.ubuntu.com/ubuntu bionic/universe amd64 elinks amd64 0.12~pre6-13 [530 kB]
debconf: unable to initialize frontend: Dialog
debconf: (Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal.)
debconf: falling back to frontend: Readline
debconf: unable to initialize frontend: Readline
debconf: (This frontend requires a controlling tty.)
debconf: falling back to frontend: Teletype
dpkg-preconfigure: unable to re-open stdin: 
Fetched 1,062 kB in 0s (2,533 kB/s)
Selecting previously unselected package libfsplib0.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 62241 files and directories currently installed.)
Preparing to unpack .../libfsplib0_0.11-2_amd64.deb ...
Unpacking libfsplib0 (0.11-2) ...
Selecting previously unselected package liblua5.1-0:amd64.
Preparing to unpack .../liblua5.1-0_5.1.5-8.1build2_amd64.deb ...
Unpacking liblua5.1-0:amd64 (5.1.5-8.1build2) ...
Selecting previously unselected package libtre5:amd64.
Preparing to unpack .../libtre5_0.8.0-6_amd64.deb ...
Unpacking libtre5:amd64 (0.8.0-6) ...
Selecting previously unselected package elinks-data.
Preparing to unpack .../elinks-data_0.12~pre6-13_all.deb ...
Unpacking elinks-data (0.12~pre6-13) ...
Selecting previously unselected package elinks.
Preparing to unpack .../elinks_0.12~pre6-13_amd64.deb ...
Unpacking elinks (0.12~pre6-13) ...
Setting up elinks-data (0.12~pre6-13) ...
Setting up libtre5:amd64 (0.8.0-6) ...
Setting up libfsplib0 (0.11-2) ...
Setting up liblua5.1-0:amd64 (5.1.5-8.1build2) ...
Setting up elinks (0.12~pre6-13) ...
Processing triggers for libc-bin (2.27-3ubuntu1) ...
Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
Processing triggers for mime-support (3.60ubuntu1) ...
```

check if `docker` and `docker-compose` commands are present

```sh
docker --version
docker-compose --version
```

```output
Docker version 19.03.6, build 369ce74a3c
docker-compose version 1.17.1, build unknown
```

## Basics

Container - in Linux world, it is a run-time environment to run a program or
entire system (init process) like Ubuntu or Fedora. Environment is isolated from
host environment. Isolation is made above the kernel - process running in the
container shares only Linux kernel with the host. This is software isolation not
security isolation. Often root user inside the container has the same privileges
as hosts root, although container provides some isolation from host in security
point of view, it is not advised to depend on it. Any attack on kernel is
potentially way to breakout from the container. Much higher level of isolation
can be achieved with virtualization.

Containers provides whole software stack that is independent form host system,
that way an application in the container can have own software stack in required
versions.

From user point of view containers might be viewed as virtualization technology
but implementation is totally different - Container is more like advanced chroot.

### Docker glossary

Docker Image - is an image with filesystem. Docker uses layered filesystem
format AUFS. Layers are combined into logically single filesystem. Image is
defined by *Dockerfile*, each line in Dockerfile creates a layer of the image.
First line in Dockerfile is `FROM` command witch create base layer from other
image, typically some base system like Ubuntu or Alpine or it can be some of
your carefully crafted system. There is only one copy of such layer and if
needed in multiple images it is shared between images. Each line in Dockerfile
creates separate layer of the image, layers other than `FROM` are not shared,
but they are still separate layers so the Image can be rebuilt very fast - only
changed layers must be rebuilt. Image is a read-only filesystem.

Docker Container is a run-time configured to run a specific process in
environment created by specific image. Because image is a read-only filesystem,
containers filesystem is created by layering writable layer on top of an image.
That way you can create many containers from the same image and no write
operation in one container collide with other container - each container has own
writable layer.

> Docker Registry - TODO

## Run a service in the container

```sh
docker run \
        --publish 5001:5432 \
        --volume pg0_data:/var/lib/postgresql/data \
        --name pg0 \
        --detach \
        --env POSTGRES_PASSWORD=M666yfVv6zAg6G \
        postgres:alpine
```

```output
Unable to find image 'postgres:alpine' locally
alpine: Pulling from library/postgres
c9b1b535fdd9: Pulling fs layer
d1030c456d04: Pulling fs layer
d1d0211bbd9a: Pulling fs layer
b66b1935d7a5: Pulling fs layer
397f12f377fd: Pulling fs layer
7dfaf92528fb: Pulling fs layer
668e00b20200: Pulling fs layer
c45112dbfcad: Pulling fs layer
b66b1935d7a5: Waiting
397f12f377fd: Waiting
7dfaf92528fb: Waiting
668e00b20200: Waiting
c45112dbfcad: Waiting
d1d0211bbd9a: Verifying Checksum
d1d0211bbd9a: Download complete
d1030c456d04: Verifying Checksum
d1030c456d04: Download complete
c9b1b535fdd9: Download complete
397f12f377fd: Verifying Checksum
397f12f377fd: Download complete
7dfaf92528fb: Verifying Checksum
7dfaf92528fb: Download complete
c9b1b535fdd9: Pull complete
d1030c456d04: Pull complete
d1d0211bbd9a: Pull complete
668e00b20200: Verifying Checksum
668e00b20200: Download complete
c45112dbfcad: Verifying Checksum
c45112dbfcad: Download complete
b66b1935d7a5: Verifying Checksum
b66b1935d7a5: Download complete
b66b1935d7a5: Pull complete
397f12f377fd: Pull complete
7dfaf92528fb: Pull complete
668e00b20200: Pull complete
c45112dbfcad: Pull complete
Digest: sha256:9d5337324064ad8e9d7de3bd35c366c11c3968c62abe43c38112be2f31f811d3
Status: Downloaded newer image for postgres:alpine
782499e2c77fd1d1a4d82bb338a2f818387a4771d9bd28806f4903b82bdfa616
```

Domain snapshot pg_container created
Postgresql service is up and running.

Running containers list:

```sh
docker ps
```

```output
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
782499e2c77f        postgres:alpine     "docker-entrypoint.s…"   17 seconds ago      Up 15 seconds       0.0.0.0:5001->5432/tcp   pg0
```

Images list:

```sh
docker image ls
```

```output
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
postgres            alpine              b876cafe1859        3 days ago          154MB
```

Volumes list:

```sh
docker volume ls
```

```output
DRIVER              VOLUME NAME
local               pg0_data
```

To connect to Postgres directly via host, find it IP, with inspect command:
`docker inspect pg0`
or use specific command:
`docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' pg0`

```sh
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' pg0
```

```output
172.17.0.2
```

Let's try to connect to posgresql

```sh
psql --user postgres \
      --host 172.17.0.2 \
      --port 5432 \
      --command 'select 1 as One;'\
      postgres
```

```output
 one 
-----
   1
(1 row)

```

Although it is possible to connect to the container by its IP address, it is
also possible to connect to the Postgres on localhost via forwarded port 5001

```sh
psql --user postgres \
      --host localhost \
      --port 5001 \
      --command 'select 2 as Two;'\
      postgres
```

```output
 two 
-----
   2
(1 row)

```

### Another example - Redis

```sh
docker run --publish 6001:6379 \
            --name redis0 \
            --detach \
        redis:alpine
```

```output
Unable to find image 'redis:alpine' locally
alpine: Pulling from library/redis
c9b1b535fdd9: Already exists
8dd5e7a0ba4a: Pulling fs layer
e20c1cdf5aef: Pulling fs layer
f06a0c1e566e: Pulling fs layer
230b5c8df708: Pulling fs layer
0cb9ac88f5bf: Pulling fs layer
230b5c8df708: Waiting
0cb9ac88f5bf: Waiting
8dd5e7a0ba4a: Verifying Checksum
8dd5e7a0ba4a: Download complete
e20c1cdf5aef: Verifying Checksum
e20c1cdf5aef: Download complete
8dd5e7a0ba4a: Pull complete
f06a0c1e566e: Verifying Checksum
f06a0c1e566e: Download complete
e20c1cdf5aef: Pull complete
230b5c8df708: Verifying Checksum
230b5c8df708: Download complete
0cb9ac88f5bf: Verifying Checksum
0cb9ac88f5bf: Download complete
f06a0c1e566e: Pull complete
230b5c8df708: Pull complete
0cb9ac88f5bf: Pull complete
Digest: sha256:cb9783b1c39bb34f8d6572406139ab325c4fac0b28aaa25d5350495637bb2f76
Status: Downloaded newer image for redis:alpine
8f82839c24d885db43335ba9b18762ee8f9f2117dc21779de39bc5083e0433dd
```

Domain snapshot redis_container created
Connect to it.

```sh
redis-cli -p 6001 ping "Hello World!"
```

```output
Hello World!
```

## build image for your app

Prepare a demo Rails project

```sh
git clone https://gitlab.com/mariusz-kowalski/rails-sidekiq.git demo_rails
```

```output
Cloning into 'demo_rails'...
```

Domain snapshot clone_repo created
```sh
cd demo_rails
```

create *Dockerfile* file in root of the project

```Dockerfile
FROM ruby:2.5.1-alpine

ENV BUNDLER_VERSION=2.0.2

RUN apk add --update --no-cache binutils-gold build-base curl file g++ gcc git \
      less libstdc++ libffi-dev libc-dev linux-headers libxml2-dev libxslt-dev \
      libgcrypt-dev make netcat-openbsd nodejs openssl pkgconfig postgresql-dev \
      python tzdata yarn postgresql-client

RUN gem install bundler -v 2.0.2

WORKDIR /app

VOLUME tmp:/app/tmp:tmpfs
VOLUME storage:/app/storage
VOLUME node_modules:/app/node_modules

COPY Gemfile Gemfile.lock ./
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle check || bundle install

COPY package.json yarn.lock ./
RUN yarn install --check-files

COPY . ./

EXPOSE 3000

CMD bundle exec rails server --binding 0.0.0.0
```

To build this image, run `docker build` command:

```sh
docker build --tag=demo-rails .
```

```output
free(): invalid pointer
SIGABRT: abort
PC=0x7f4f7c257e97 m=0 sigcode=18446744073709551610
signal arrived during cgo execution

goroutine 1 [syscall, locked to thread]:
runtime.cgocall(0x4afd50, 0xc420045cc0, 0xc420045ce8)
	/usr/lib/go-1.8/src/runtime/cgocall.go:131 +0xe2 fp=0xc420045c90 sp=0xc420045c50
github.com/docker/docker-credential-helpers/secretservice._Cfunc_free(0x1e88270)
	github.com/docker/docker-credential-helpers/secretservice/_obj/_cgo_gotypes.go:111 +0x41 fp=0xc420045cc0 sp=0xc420045c90
github.com/docker/docker-credential-helpers/secretservice.Secretservice.List.func5(0x1e88270)
	/build/golang-github-docker-docker-credential-helpers-cMhSy1/golang-github-docker-docker-credential-helpers-0.5.0/obj-x86_64-linux-gnu/src/github.com/docker/docker-credential-helpers/secretservice/secretservice_linux.go:96 +0x60 fp=0xc420045cf8 sp=0xc420045cc0
github.com/docker/docker-credential-helpers/secretservice.Secretservice.List(0x0, 0x756060, 0xc420012340)
	/build/golang-github-docker-docker-credential-helpers-cMhSy1/golang-github-docker-docker-credential-helpers-0.5.0/obj-x86_64-linux-gnu/src/github.com/docker/docker-credential-helpers/secretservice/secretservice_linux.go:97 +0x217 fp=0xc420045da0 sp=0xc420045cf8
github.com/docker/docker-credential-helpers/secretservice.(*Secretservice).List(0x77e548, 0xc420045e88, 0x410022, 0xc420012290)
	<autogenerated>:4 +0x46 fp=0xc420045de0 sp=0xc420045da0
github.com/docker/docker-credential-helpers/credentials.List(0x756ba0, 0x77e548, 0x7560e0, 0xc42000e018, 0x0, 0x10)
	/build/golang-github-docker-docker-credential-helpers-cMhSy1/golang-github-docker-docker-credential-helpers-0.5.0/obj-x86_64-linux-gnu/src/github.com/docker/docker-credential-helpers/credentials/credentials.go:145 +0x3e fp=0xc420045e68 sp=0xc420045de0
github.com/docker/docker-credential-helpers/credentials.HandleCommand(0x756ba0, 0x77e548, 0x7ffcb5eb2d51, 0x4, 0x7560a0, 0xc42000e010, 0x7560e0, 0xc42000e018, 0x40e398, 0x4d35c0)
	/build/golang-github-docker-docker-credential-helpers-cMhSy1/golang-github-docker-docker-credential-helpers-0.5.0/obj-x86_64-linux-gnu/src/github.com/docker/docker-credential-helpers/credentials/credentials.go:60 +0x16d fp=0xc420045ed8 sp=0xc420045e68
github.com/docker/docker-credential-helpers/credentials.Serve(0x756ba0, 0x77e548)
	/build/golang-github-docker-docker-credential-helpers-cMhSy1/golang-github-docker-docker-credential-helpers-0.5.0/obj-x86_64-linux-gnu/src/github.com/docker/docker-credential-helpers/credentials/credentials.go:41 +0x1cb fp=0xc420045f58 sp=0xc420045ed8
main.main()
	/build/golang-github-docker-docker-credential-helpers-cMhSy1/golang-github-docker-docker-credential-helpers-0.5.0/secretservice/cmd/main_linux.go:9 +0x4f fp=0xc420045f88 sp=0xc420045f58
runtime.main()
	/usr/lib/go-1.8/src/runtime/proc.go:185 +0x20a fp=0xc420045fe0 sp=0xc420045f88
runtime.goexit()
	/usr/lib/go-1.8/src/runtime/asm_amd64.s:2197 +0x1 fp=0xc420045fe8 sp=0xc420045fe0

goroutine 17 [syscall, locked to thread]:
runtime.goexit()
	/usr/lib/go-1.8/src/runtime/asm_amd64.s:2197 +0x1

rax    0x0
rbx    0x7ffcb5eb1370
rcx    0x7f4f7c257e97
rdx    0x0
rdi    0x2
rsi    0x7ffcb5eb1100
rbp    0x7ffcb5eb1470
rsp    0x7ffcb5eb1100
r8     0x0
r9     0x7ffcb5eb1100
r10    0x8
r11    0x246
r12    0x7ffcb5eb1370
r13    0x1000
r14    0x0
r15    0x30
rip    0x7f4f7c257e97
rflags 0x246
cs     0x33
fs     0x0
gs     0x0
Sending build context to Docker daemon  537.1kB
Step 1/16 : FROM ruby:2.5.1-alpine
2.5.1-alpine: Pulling from library/ruby
c67f3896b22c: Pulling fs layer
4ce14b0d7247: Pulling fs layer
98cc3898e899: Pulling fs layer
f8ef96ffa528: Pulling fs layer
f8ef96ffa528: Waiting
4ce14b0d7247: Verifying Checksum
4ce14b0d7247: Download complete
c67f3896b22c: Verifying Checksum
c67f3896b22c: Download complete
c67f3896b22c: Pull complete
f8ef96ffa528: Verifying Checksum
f8ef96ffa528: Download complete
98cc3898e899: Verifying Checksum
98cc3898e899: Download complete
4ce14b0d7247: Pull complete
98cc3898e899: Pull complete
f8ef96ffa528: Pull complete
Digest: sha256:5c2c66e70bfbb3f08d67e1335f555ccf69f6e3bcbf418a7b2fe3abf31d330ff2
Status: Downloaded newer image for ruby:2.5.1-alpine
 ---> d29267791323
Step 2/16 : ENV BUNDLER_VERSION=2.0.2
 ---> Running in 03a4bb4f6920
Removing intermediate container 03a4bb4f6920
 ---> 194095162037
Step 3/16 : RUN apk add --update --no-cache binutils-gold build-base curl file g++ gcc git       less libstdc++ libffi-dev libc-dev linux-headers libxml2-dev libxslt-dev       libgcrypt-dev make netcat-openbsd nodejs openssl pkgconfig postgresql-dev       python tzdata yarn postgresql-client
 ---> Running in 57707807bbd0
fetch http://dl-cdn.alpinelinux.org/alpine/v3.7/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.7/community/x86_64/APKINDEX.tar.gz
(1/62) Upgrading musl (1.1.18-r3 -> 1.1.18-r4)
(2/62) Installing binutils-gold (2.30-r2)
(3/62) Installing binutils-libs (2.30-r2)
(4/62) Installing binutils (2.30-r2)
(5/62) Installing gmp (6.1.2-r1)
(6/62) Installing isl (0.18-r0)
(7/62) Installing libgomp (6.4.0-r5)
(8/62) Installing libatomic (6.4.0-r5)
(9/62) Installing libgcc (6.4.0-r5)
(10/62) Installing mpfr3 (3.1.5-r1)
(11/62) Installing mpc1 (1.0.3-r1)
(12/62) Installing libstdc++ (6.4.0-r5)
(13/62) Installing gcc (6.4.0-r5)
(14/62) Installing musl-dev (1.1.18-r4)
(15/62) Installing libc-dev (0.7.1-r0)
(16/62) Installing g++ (6.4.0-r5)
(17/62) Installing make (4.2.1-r0)
(18/62) Installing fortify-headers (0.9-r0)
(19/62) Installing build-base (0.5-r0)
(20/62) Installing libssh2 (1.9.0-r1)
(21/62) Installing libcurl (7.61.1-r3)
(22/62) Installing curl (7.61.1-r3)
(23/62) Installing libmagic (5.32-r2)
(24/62) Installing file (5.32-r2)
(25/62) Installing expat (2.2.8-r0)
(26/62) Installing pcre2 (10.30-r0)
(27/62) Installing git (2.15.4-r0)
(28/62) Installing less (520-r0)
(29/62) Upgrading musl-utils (1.1.18-r3 -> 1.1.18-r4)
(30/62) Installing libgpg-error (1.27-r1)
(31/62) Installing libgpg-error-dev (1.27-r1)
(32/62) Installing libgcrypt (1.8.3-r2)
(33/62) Installing libgcrypt-dev (1.8.3-r2)
(34/62) Installing libxml2 (2.9.8-r1)
(35/62) Installing libxml2-dev (2.9.8-r1)
(36/62) Installing libxslt (1.1.31-r2)
(37/62) Installing libxslt-dev (1.1.31-r2)
(38/62) Installing linux-headers (4.4.6-r2)
(39/62) Installing libbsd (0.8.6-r1)
(40/62) Installing netcat-openbsd (1.130-r1)
(41/62) Installing nodejs-npm (8.9.3-r1)
(42/62) Installing c-ares (1.13.0-r0)
(43/62) Installing libcrypto1.0 (1.0.2t-r0)
(44/62) Installing http-parser (2.7.1-r1)
(45/62) Installing libssl1.0 (1.0.2t-r0)
(46/62) Installing libuv (1.17.0-r0)
(47/62) Installing nodejs (8.9.3-r1)
(48/62) Installing openssl (1.0.2t-r0)
(49/62) Installing libedit (20170329.3.1-r3)
(50/62) Installing db (5.3.28-r0)
(51/62) Installing libsasl (2.1.26-r11)
(52/62) Installing libldap (2.4.48-r0)
(53/62) Installing libpq (10.10-r0)
(54/62) Installing postgresql-client (10.10-r0)
(55/62) Installing libressl-dev (2.6.5-r0)
(56/62) Installing postgresql-libs (10.10-r0)
(57/62) Installing postgresql-dev (10.10-r0)
(58/62) Installing libbz2 (1.0.6-r7)
(59/62) Installing sqlite-libs (3.25.3-r2)
(60/62) Installing python2 (2.7.15-r3)
(61/62) Installing tzdata (2019c-r0)
(62/62) Installing yarn (1.3.2-r0)
Executing busybox-1.27.2-r11.trigger
Executing ca-certificates-20171114-r0.trigger
OK: 332 MiB in 90 packages
Removing intermediate container 57707807bbd0
 ---> 3449c36c6e00
Step 4/16 : RUN gem install bundler -v 2.0.2
 ---> Running in e864201617af
Successfully installed bundler-2.0.2
1 gem installed
Removing intermediate container e864201617af
 ---> 0bf37e6a2b85
Step 5/16 : WORKDIR /app
 ---> Running in 4b29b584407d
Removing intermediate container 4b29b584407d
 ---> f5a368321f8b
Step 6/16 : VOLUME tmp:/app/tmp:tmpfs
 ---> Running in 2815cdd35a17
Removing intermediate container 2815cdd35a17
 ---> 35145f3b72fc
Step 7/16 : VOLUME storage:/app/storage
 ---> Running in 70551cf96256
Removing intermediate container 70551cf96256
 ---> c05a703e371e
Step 8/16 : VOLUME node_modules:/app/node_modules
 ---> Running in e940f9c4a748
Removing intermediate container e940f9c4a748
 ---> d3015d59cd2a
Step 9/16 : COPY Gemfile Gemfile.lock ./
 ---> e2c54ced799c
Step 10/16 : RUN bundle config build.nokogiri --use-system-libraries
 ---> Running in 3e93acfc7fea
Removing intermediate container 3e93acfc7fea
 ---> 7019b6704e43
Step 11/16 : RUN bundle check || bundle install
 ---> Running in 1def3f4cf65b
[91mThe dependency tzinfo-data (>= 0) will be unused by any of the platforms Bundler is installing for. Bundler is installing for ruby but the dependency is only for x86-mingw32, x86-mswin32, x64-mingw32, java. To add those platforms to the bundle, run `bundle lock --add-platform x86-mingw32 x86-mswin32 x64-mingw32 java`.
[0m[91mThe following gems are missing
 * rake (12.3.3)
[0m[91m * concurrent-ruby (1.1.5)
 * i18n (1.6.0)
 * minitest (5.12.0)
 * thread_safe (0.3.6)
 * tzinfo (1.2.5)
 * activesupport (5.2.3)
 * builder (3.2.3)
 * erubi (1.9.0)
 * mini_portile2 (2.4.0)
[0m[91m * nokogiri (1.10.5)
 * rails-dom-testing (2.0.3)
[0m[91m * crass (1.0.5)
 * loofah (2.3.1)
 * rails-html-sanitizer (1.2.0)
 * actionview (5.2.3)
[0m[91m * rack (2.0.8)
 * rack-test (1.1.0)
 * actionpack (5.2.3)
 * nio4r (2.5.2)
[0m[91m * websocket-extensions (0.1.4)
 * websocket-driver (0.7.1)
[0m[91m * actioncable (5.2.3)
 * globalid (0.4.2)
 * activejob (5.2.3)
 * mini_mime (1.0.2)
[0m[91m * mail (2.7.1)
 * actionmailer (5.2.3)
 * activemodel (5.2.3)
[0m[91m * arel (9.0.0)
 * activerecord (5.2.3)
 * mimemagic (0.3.3)
 * marcel (0.3.3)
 * activestorage (5.2.3)
 * public_suffix (4.0.1)
[0m[91m * addressable (2.7.0)
 * io-like (0.3.0)
 * archive-zip (0.12.0)
 * bindex (0.8.1)
[0m[91m * msgpack (1.3.1)
 * bootsnap (1.4.5)
 * byebug (11.0.1)
 * regexp_parser (1.6.0)
 * xpath (3.2.0)
[0m[91m * capybara (3.29.0)
 * childprocess (2.0.0)
 * chromedriver-helper (2.1.1)
[0m[91m * coffee-script-source (1.12.2)
 * execjs (2.7.0)
 * coffee-script (2.4.1)
[0m[91m * method_source (0.9.2)
 * thor (0.20.3)
 * railties (5.2.3)
 * coffee-rails (4.2.2)
[0m[91m * connection_pool (2.2.2)
 * ffi (1.11.1)
 * font-awesome-rails (4.7.0.5)
[0m[91m * jbuilder (2.9.1)
 * rb-fsevent (0.10.3)
 * rb-inotify (0.10.0)
 * ruby_dep (1.5.0)
[0m[91m * listen (3.1.5)
 * pg (1.1.4)
 * puma (3.12.2)
[0m[91m * rack-protection (2.0.7)
 * rack-proxy (0.6.5)
 * sprockets (3.7.2)
 * sprockets-rails (3.2.1)
[0m[91m * rails (5.2.3)
 * redis (4.1.3)
 * rubyzip (1.3.0)
 * sass-listen (4.0.0)
 * sass (3.7.4)
 * tilt (2.0.10)
 * sass-rails (5.1.0)
 * selenium-webdriver (3.142.4)
 * sidekiq (6.0.3)
 * spring (2.1.0)
[0m[91m * turbolinks-source (5.2.0)
 * turbolinks (5.2.1)
 * uglifier (4.2.0)
 * web-console (3.7.0)
 * webpacker (4.0.7)
Install missing gems with `bundle install`
[0m[91mThe dependency tzinfo-data (>= 0) will be unused by any of the platforms Bundler is installing for. Bundler is installing for ruby but the dependency is only for x86-mingw32, x86-mswin32, x64-mingw32, java. To add those platforms to the bundle, run `bundle lock --add-platform x86-mingw32 x86-mswin32 x64-mingw32 java`.
[0mFetching gem metadata from https://rubygems.org/............
Fetching rake 12.3.3
Installing rake 12.3.3
Fetching concurrent-ruby 1.1.5
Installing concurrent-ruby 1.1.5
Fetching i18n 1.6.0
Installing i18n 1.6.0
Fetching minitest 5.12.0
Installing minitest 5.12.0
Fetching thread_safe 0.3.6
Installing thread_safe 0.3.6
Fetching tzinfo 1.2.5
Installing tzinfo 1.2.5
Fetching activesupport 5.2.3
Installing activesupport 5.2.3
Fetching builder 3.2.3
Installing builder 3.2.3
Fetching erubi 1.9.0
Installing erubi 1.9.0
Fetching mini_portile2 2.4.0
Installing mini_portile2 2.4.0
Fetching nokogiri 1.10.5
Installing nokogiri 1.10.5 with native extensions
Fetching rails-dom-testing 2.0.3
Installing rails-dom-testing 2.0.3
Fetching crass 1.0.5
Installing crass 1.0.5
Fetching loofah 2.3.1
Installing loofah 2.3.1
Fetching rails-html-sanitizer 1.2.0
Installing rails-html-sanitizer 1.2.0
Fetching actionview 5.2.3
Installing actionview 5.2.3
Fetching rack 2.0.8
Installing rack 2.0.8
Fetching rack-test 1.1.0
Installing rack-test 1.1.0
Fetching actionpack 5.2.3
Installing actionpack 5.2.3
Fetching nio4r 2.5.2
Installing nio4r 2.5.2 with native extensions
Fetching websocket-extensions 0.1.4
Installing websocket-extensions 0.1.4
Fetching websocket-driver 0.7.1
Installing websocket-driver 0.7.1 with native extensions
Fetching actioncable 5.2.3
Installing actioncable 5.2.3
Fetching globalid 0.4.2
Installing globalid 0.4.2
Fetching activejob 5.2.3
Installing activejob 5.2.3
Fetching mini_mime 1.0.2
Installing mini_mime 1.0.2
Fetching mail 2.7.1
Installing mail 2.7.1
Fetching actionmailer 5.2.3
Installing actionmailer 5.2.3
Fetching activemodel 5.2.3
Installing activemodel 5.2.3
Fetching arel 9.0.0
Installing arel 9.0.0
Fetching activerecord 5.2.3
Installing activerecord 5.2.3
Fetching mimemagic 0.3.3
Installing mimemagic 0.3.3
Fetching marcel 0.3.3
Installing marcel 0.3.3
Fetching activestorage 5.2.3
Installing activestorage 5.2.3
Fetching public_suffix 4.0.1
Installing public_suffix 4.0.1
Fetching addressable 2.7.0
Installing addressable 2.7.0
Fetching io-like 0.3.0
Installing io-like 0.3.0
Fetching archive-zip 0.12.0
Installing archive-zip 0.12.0
Fetching bindex 0.8.1
Installing bindex 0.8.1 with native extensions
Fetching msgpack 1.3.1
Installing msgpack 1.3.1 with native extensions
Fetching bootsnap 1.4.5
Installing bootsnap 1.4.5 with native extensions
Using bundler 2.0.2
Fetching byebug 11.0.1
Installing byebug 11.0.1 with native extensions
Fetching regexp_parser 1.6.0
Installing regexp_parser 1.6.0
Fetching xpath 3.2.0
Installing xpath 3.2.0
Fetching capybara 3.29.0
Installing capybara 3.29.0
Fetching childprocess 2.0.0
Installing childprocess 2.0.0 with native extensions
Fetching chromedriver-helper 2.1.1
Installing chromedriver-helper 2.1.1
Fetching coffee-script-source 1.12.2
Installing coffee-script-source 1.12.2
Fetching execjs 2.7.0
Installing execjs 2.7.0
Fetching coffee-script 2.4.1
Installing coffee-script 2.4.1
Fetching method_source 0.9.2
Installing method_source 0.9.2
Fetching thor 0.20.3
Installing thor 0.20.3
Fetching railties 5.2.3
Installing railties 5.2.3
Fetching coffee-rails 4.2.2
Installing coffee-rails 4.2.2
Fetching connection_pool 2.2.2
Installing connection_pool 2.2.2
Fetching ffi 1.11.1
Installing ffi 1.11.1 with native extensions
Fetching font-awesome-rails 4.7.0.5
Installing font-awesome-rails 4.7.0.5
Fetching jbuilder 2.9.1
Installing jbuilder 2.9.1
Fetching rb-fsevent 0.10.3
Installing rb-fsevent 0.10.3
Fetching rb-inotify 0.10.0
Installing rb-inotify 0.10.0
Fetching ruby_dep 1.5.0
Installing ruby_dep 1.5.0
Fetching listen 3.1.5
Installing listen 3.1.5
Fetching pg 1.1.4
Installing pg 1.1.4 with native extensions
Fetching puma 3.12.2
Installing puma 3.12.2 with native extensions
Fetching rack-protection 2.0.7
Installing rack-protection 2.0.7
Fetching rack-proxy 0.6.5
Installing rack-proxy 0.6.5
Fetching sprockets 3.7.2
Installing sprockets 3.7.2
Fetching sprockets-rails 3.2.1
Installing sprockets-rails 3.2.1
Fetching rails 5.2.3
Installing rails 5.2.3
Fetching redis 4.1.3
Installing redis 4.1.3
Fetching rubyzip 1.3.0
Installing rubyzip 1.3.0
Fetching sass-listen 4.0.0
Installing sass-listen 4.0.0
Fetching sass 3.7.4
Installing sass 3.7.4
Fetching tilt 2.0.10
Installing tilt 2.0.10
Fetching sass-rails 5.1.0
Installing sass-rails 5.1.0
Fetching selenium-webdriver 3.142.4
Installing selenium-webdriver 3.142.4
Fetching sidekiq 6.0.3
Installing sidekiq 6.0.3
Fetching spring 2.1.0
Installing spring 2.1.0
Fetching turbolinks-source 5.2.0
Installing turbolinks-source 5.2.0
Fetching turbolinks 5.2.1
Installing turbolinks 5.2.1
Fetching uglifier 4.2.0
Installing uglifier 4.2.0
Fetching web-console 3.7.0
Installing web-console 3.7.0
Fetching webpacker 4.0.7
Installing webpacker 4.0.7
Bundle complete! 20 Gemfile dependencies, 84 gems now installed.
Bundled gems are installed into `/usr/local/bundle`
Post-install message from i18n:

HEADS UP! i18n 1.1 changed fallbacks to exclude default locale.
But that may break your application.

Please check your Rails app for 'config.i18n.fallbacks = true'.
If you're using I18n (>= 1.1.0) and Rails (< 5.2.2), this should be
'config.i18n.fallbacks = [I18n.default_locale]'.
If not, fallbacks will be broken in your app by I18n 1.1.x.

For more info see:
https://github.com/svenfuchs/i18n/releases/tag/v1.1.0

Post-install message from chromedriver-helper:

  +--------------------------------------------------------------------+
  |                                                                    |
  |  NOTICE: chromedriver-helper is deprecated after 2019-03-31.       |
  |                                                                    |
  |  Please update to use the 'webdrivers' gem instead.                |
  |  See https://github.com/flavorjones/chromedriver-helper/issues/83  |
  |                                                                    |
  +--------------------------------------------------------------------+

Post-install message from sass:

Ruby Sass has reached end-of-life and should no longer be used.

* If you use Sass as a command-line tool, we recommend using Dart Sass, the new
  primary implementation: https://sass-lang.com/install

* If you use Sass as a plug-in for a Ruby web framework, we recommend using the
  sassc gem: https://github.com/sass/sassc-ruby#readme

* For more details, please refer to the Sass blog:
  https://sass-lang.com/blog/posts/7828841

Removing intermediate container 1def3f4cf65b
 ---> 01058945f107
Step 12/16 : COPY package.json yarn.lock ./
 ---> a5413cd32178
Step 13/16 : RUN yarn install --check-files
 ---> Running in 491734263ede
yarn install v1.3.2
[1/4] Resolving packages...
[2/4] Fetching packages...
info fsevents@1.2.9: The platform "linux" is incompatible with this module.
info "fsevents@1.2.9" is an optional dependency and failed compatibility check. Excluding it from installation.
[3/4] Linking dependencies...
[91mwarning "@rails/webpacker > pnp-webpack-plugin > ts-pnp@1.1.4" has unmet peer dependency "typescript@*".
[0m[91mwarning " > webpack-dev-server@3.8.1" has unmet peer dependency "webpack@^4.0.0".
[0m[91mwarning "webpack-dev-server > webpack-dev-middleware@3.7.1" has unmet peer dependency "webpack@^4.0.0".
[0m[4/4] Building fresh packages...
Done in 22.47s.
Removing intermediate container 491734263ede
 ---> 00302016d3da
Step 14/16 : COPY . ./
 ---> 382c3bdc5898
Step 15/16 : EXPOSE 3000
 ---> Running in 4c92193ec426
Removing intermediate container 4c92193ec426
 ---> c10224ad05a9
Step 16/16 : CMD bundle exec rails server --binding 0.0.0.0
 ---> Running in ba680fb1b662
Removing intermediate container ba680fb1b662
 ---> c848b78f8bb3
Successfully built c848b78f8bb3
Successfully tagged demo-rails:latest
```

Domain snapshot app_container_build created
To see list of images:

```sh
docker image list
```

```output
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
demo-rails          latest              c848b78f8bb3        27 seconds ago      610MB
postgres            alpine              b876cafe1859        3 days ago          154MB
redis               alpine              b68707e68547        4 weeks ago         29.8MB
ruby                2.5.1-alpine        d29267791323        16 months ago       45.3MB
```

Create container from this image

```sh
docker create --name demo-rails-app \
               --network host \
               --env-file .env-docker \
        demo-rails
```

```output
79dcc4e7ddec22d7affe7e3d39678640e4f1eeb1adb52cfdd06fb85e6d15b542
```

Option `--network host` configures the container that way it uses hosts network.
All services run in the container all available on hosts localhost. In this case
container serves a web page on port 3000 which is available as it would be run
on local machine - (localhost:3000)[http://localhost:3000].

Option `--env-file` will add environment variables in the container from given
file, you may need to adjust it.

./docker_intro.md.sh: line 204: POSTGRES_PASSWORD=M666yfVv6zAg6G: command not found
To run a container:

```sh
docker start demo-rails-app
```

```output
demo-rails-app
```

To list containers

```sh
docker ps --all
```

```output
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                  PORTS                    NAMES
79dcc4e7ddec        demo-rails          "/bin/sh -c 'bundle …"   2 seconds ago       Up Less than a second                            demo-rails-app
8f82839c24d8        redis:alpine        "docker-entrypoint.s…"   3 minutes ago       Up 3 minutes            0.0.0.0:6001->6379/tcp   redis0
782499e2c77f        postgres:alpine     "docker-entrypoint.s…"   3 minutes ago       Up 3 minutes            0.0.0.0:5001->5432/tcp   pg0
```

`--all`, `-a` - prints also stopped containers.

To see logs from the container:

```sh
docker logs demo-rails-app
```

```output
```

Option `--follow` (`-f`) will prints logs in real time.

Command `docker run` is a combination of `create` and `start` commands.
Equivalent of the above tho commands would be:

```
docker run --name demo-rails-app --publish 3000:3000 demo-rails
```

Command `run` is especially useful for creating one time use containers, with
different command to run - just add the command at the end with options - it
will override the default `CMD` line from Dockerfile

```sh
docker run --rm demo-rails bundle exec rails --version
```

```output
Rails 5.2.3
```

This command will create a new container, with some random name, it will
execute `bundle exec rails --versions` instead of the one defined in the
Dockerfile. After the command ends, the container will stop itself and delete
itself (option `--rm`)

To run a shell in one time container, use options `--interactive` and `--tty`
(short version `-it`):

```sh
docker run --rm --interactive --tty demo-rails sh
```

This should run into interactive shell.

## Development flow of the application with the container


--end--
