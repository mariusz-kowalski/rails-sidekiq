#!/bin/bash

RUNNER='sh'
VM_NAME='md5'
VM_IP=''

RESULT=0

function h1 {
  echo "# $@"
  echo
}

function h2 {
  echo "## $@"
  echo
}

function h3 {
  echo "### $@"
  echo
}

function h4 {
  echo "#### $@"
  echo
}

function p {
  echo "$@"
  echo
}

function ul {
  for li in "$@"
  do
    echo "* $li"
  done
  echo
}

function comment {
  echo "> $@"
  echo
}

function code {
  echo "\`\`\`$2"
  echo "$1"
  echo '```'
  echo
}

function file {
  code "$(run cat "$1")" $2
}

function edit_file {
  sed -i "s/$2/$3/" "$1"
}

function sh {
  code "$1" "sh"

  # command=${1//\\$'\n'/' '}
  # $2 additional env vars
  command="$2 $1"
  # echo "$command" # debug
  echo '```output'
  run "$command"
  echo '```'
  echo
}

function sh {
  code "$1" "sh"

  # command=${1//\\$'\n'/' '}
  # $2 additional env vars
  command="$2 $1"
  # echo "$command" # debug
  echo '```output'
  run "$command"
  echo '```'
  echo

  if [ $RESULT != 0 ]; then
    echo "Command '$@' returned error - $RESULT." >&2
    exit 1
  fi
}

function cd {
  CD="$1"
  code "cd $CD" 'sh'
}

function run {
  # silently run command
  case $RUNNER in
    'sh')
      "$@"
    ;;
    'vm')
      ssh_run "$@"
    ;;
  esac
}

function ssh_run {
  # add key to known_hosts for the first time
  if [ -z "$(ssh-keygen -F $VM_IP)" ]; then
    ssh-keyscan -H $VM_IP >> ~/.ssh/known_hosts
  fi

  command="$@"
  if [ -n "$CD" ]; then
    command="cd '$CD'; $command"
  fi
  LC_ALL=en_US.UTF-8 ssh ubuntu@$VM_IP "$command"
  RESULT=$?
}

function silent_run {
  run "$@" > /dev/null 2>&1
}

# function run {
#   run "$@"
#   if ! $?; then
#     echo "Command '$@' returned error."
#     exit 1
#   fi
# }

function runner {
  RUNNER=$1
  case $RUNNER in
    'sh')
      $@
    ;;
    'vm')
      prepare_vm "$2"
    ;;
  esac
}

function prepare_vm {
  if sudo uvt-kvm list | grep $VM_NAME > /dev/null 2>&1
  then
    # echo "VM $VM_NAME exists, restore the initial snapshot..."
    # FIXME: snapshot may not exist
    set_vm_ip
    if [ "$(vm_state)" != 'running' ]; then
      sudo virsh start $VM_NAME
      set_vm_ip
    fi
    if echo "$(snapshot_list)" | grep 'initial' > /dev/null 2>&1; then
      revert_snapshot 'initial'
    fi
  else
    sudo uvt-kvm create --ssh-public-key-file ~/.ssh/id_rsa.pub \
                        --memory 4092 \
                        --cpu 2 \
        $VM_NAME release=$1
    sudo uvt-kvm wait $VM_NAME --insecure
    #prepare OS
    # run "sudo apt --yes update"
    # run "sudo apt --yes upgrade"
    create_snapshot 'initial'
  fi

  set_vm_ip
  if [ -z "$VM_IP" ]; then
    echo "No ip for VM..."
    exit 2
  fi
}

function set_vm_ip {
  VM_IP=$(sudo uvt-kvm ip ${VM_NAME} 2> /dev/null)
}

function vm_state {
  sudo virsh domstate $VM_NAME
}

function create_snapshot {
  sudo virsh snapshot-create-as $VM_NAME --name $1
}

function revert_snapshot {
  sudo virsh snapshot-revert $VM_NAME --snapshotname $1 || exit 1
}

function snapshot {
  snapshot_name="$1"
  code="$2"
  if sudo virsh snapshot-list $VM_NAME | grep $snapshot_name > /dev/null 2>&1
  then
    # echo snapshot exists - revert
    revert_snapshot $snapshot_name
    code "$code" "sh"
  else
    # echo snapshot does not exist
    sh "$code"
    create_snapshot $snapshot_name
  fi
}

function cleanup {
  echo "Cleanup..."
  case $RUNNER in
    'sh')
      $@
    ;;
    'vm')
      # sudo virsh snapshot-delete $VM_NAME --snapshotname prepared
      # sudo virsh snapshot-delete $VM_NAME --snapshotname initial
      sudo virsh destroy $VM_NAME
      sleep 30
      for sn in $(snapshot_list); do
        sudo virsh snapshot-delete $VM_NAME --snapshotname $sn
      done
      sudo virsh undefine $VM_NAME --remove-all-storage
      # sudo virsh undefine $VM_NAME
    ;;
  esac
}

function snapshot_list {
  sudo virsh snapshot-list $VM_NAME --name
}

function teardown {
  echo
  echo --end--
}

function opts {
  case "$1" in
    '--cleanup')
      VM_IP=$(sudo uvt-kvm ip $VM_NAME) # <-- this is BS... all the time IP needs to be calculated
      cleanup
      exit 0
    ;;
  esac
}
