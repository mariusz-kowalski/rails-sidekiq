# Dev in Docker

## basics

* get some linux image.
* run command in the container
* run shell in the container
    - check `docker ps`

## service in the container

### Run postgresql in the container

```sh
docker run \
  --publish=5001:5432\
  --volume=pg0_data:/var/lib/postgresql/data\
  --name=pg0\
  --detach\
  --env POSTGRES_PASSWORD=qwe123\
  postgres:alpine
```

This creates docker container with postgresql databsse service with volume to store data, that it can be persisted if container is removed.

Although you can connect to the container by its IP address (`docker container inspect pg1` to find its IP) you can connect to the Postgres on localhost via forwarded port 5001:

```sh
psql --user postgres --host localhost --port 5002 postgres
```

### 2-nd example - Redis

```sh
docker run --publish=6001:6379 --name=redis0 --detach redis:alpine
```

Connect to it.

```sh
redis-cli -p 6001
```

## build image for your app

* create `Dockerfile`

### build the image

```sh
docker build --tag=demo_rails0 .
```



* run it, (it is not fully functional yet, no db!)

## compose

* create docker compose config for your dev env
  - database
  - redis
  - web service
  - background job

* test env (with selenium)
  - utilize web app? possible?
  - create separate image? no autorun on compse up???

## test dev flow

* Make change to code (see change in the app)
* make db migration, apply the migration
* view logs (all, separate for each container)
* run rails console
* run rails dbconsole
* run rake task, rails generators, linux shell
* work with irb/pry/byebug, also on background job
* make change to the Gemfile, apply it
* sending emails - where to see it? Catching in the browser?
* run sql console

# Deploy with Docker (to Azure)

* azure docker registry
* deploy with custom containers for all the env (redis, db, bgjob)
  - test update
  - test update with db migration
  - test rollback
* CI
  - lints, test, selenium
  - per feature beta env
