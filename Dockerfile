FROM ruby:2.5.1-alpine

ENV BUNDLER_VERSION=2.0.2

RUN apk add --update --no-cache binutils-gold build-base curl file g++ gcc git \
      less libstdc++ libffi-dev libc-dev linux-headers libxml2-dev libxslt-dev \
      libgcrypt-dev make netcat-openbsd nodejs openssl pkgconfig postgresql-dev \
      python tzdata yarn postgresql-client

RUN gem install bundler -v 2.0.2

WORKDIR /app

VOLUME tmp:/app/tmp:tmpfs
VOLUME storage:/app/storage
VOLUME node_modules:/app/node_modules

COPY Gemfile Gemfile.lock ./
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle check || bundle install

COPY package.json yarn.lock ./
RUN yarn install --check-files

COPY . ./

EXPOSE 3000

CMD bundle exec rails server --binding 0.0.0.0
