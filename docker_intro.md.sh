#!/usr/bin/env bash

source md.sh.sh
UBUNTU_VERSION='bionic'
runner 'vm' $UBUNTU_VERSION
opts "$@"

h1 "Rails dev in Docker"
h2 "Requirenmets"

ul "OS: Ubuntu" \
   "version: $UBUNTU_VERSION"

h2 'Resources'

ul '[docker reference documentation](https://docs.docker.com/reference/)'

p "Prepareing docker environment."

snapshot 'prepared' \
'sudo apt-get --yes update
sudo apt-get --yes upgrade
sudo apt-get --yes install apt-transport-https ca-certificates curl \
                            gnupg-agent software-properties-common \
                            postgresql-client redis-tools git
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
sudo apt-get --yes update
sudo apt-get --yes install docker-ce docker-ce-cli containerd.io docker-compose
sudo adduser $USER docker'

# debugging
sh 'sudo apt-get --yes install elinks'

p 'check if `docker` and `docker-compose` commands are present'
sh \
'docker --version
docker-compose --version'

h2 'Basics'

p \
'Container - in Linux world, it is a run-time environment to run a program or
entire system (init process) like Ubuntu or Fedora. Environment is isolated from
host environment. Isolation is made above the kernel - process running in the
container shares only Linux kernel with the host. This is software isolation not
security isolation. Often root user inside the container has the same privileges
as hosts root, although container provides some isolation from host in security
point of view, it is not advised to depend on it. Any attack on kernel is
potentially way to breakout from the container. Much higher level of isolation
can be achieved with virtualization.'

p \
'Containers provides whole software stack that is independent form host system,
that way an application in the container can have own software stack in required
versions.'

p \
'From user point of view containers might be viewed as virtualization technology
but implementation is totally different - Container is more like advanced chroot.'

h3 'Docker glossary'

p \
'Docker Image - is an image with filesystem. Docker uses layered filesystem
format AUFS. Layers are combined into logically single filesystem. Image is
defined by *Dockerfile*, each line in Dockerfile creates a layer of the image.
First line in Dockerfile is `FROM` command witch create base layer from other
image, typically some base system like Ubuntu or Alpine or it can be some of
your carefully crafted system. There is only one copy of such layer and if
needed in multiple images it is shared between images. Each line in Dockerfile
creates separate layer of the image, layers other than `FROM` are not shared,
but they are still separate layers so the Image can be rebuilt very fast - only
changed layers must be rebuilt. Image is a read-only filesystem.'

p \
'Docker Container is a run-time configured to run a specific process in
environment created by specific image. Because image is a read-only filesystem,
containers filesystem is created by layering writable layer on top of an image.
That way you can create many containers from the same image and no write
operation in one container collide with other container - each container has own
writable layer.'

comment 'Docker Registry - TODO'

h2 'Run a service in the container'

pg_container='pg0'
pg_volume='pg0_data'
postgres_password='M666yfVv6zAg6G'
redis_container='redis0'

# sh \
snapshot 'pg_container' \
"docker run \\
        --publish 5001:5432 \\
        --volume $pg_volume:/var/lib/postgresql/data \\
        --name $pg_container \\
        --detach \\
        --env POSTGRES_PASSWORD=$postgres_password \\
        postgres:alpine"

p "Postgresql service is up and running."
p "Running containers list:"
sh "docker ps"

p "Images list:"
sh "docker image ls"

p "Volumes list:"
sh "docker volume ls"

container_ip_command="docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $pg_container"

p \
"To connect to Postgres directly via host, find it IP, with inspect command:
\`docker inspect $pg_container\`
or use specific command:
\`$container_ip_command\`"

sh "$container_ip_command"

container_ip="$(run "${container_ip_command}")"

p "Let's try to connect to posgresql"
sh \
"psql --user postgres \\
      --host $container_ip \\
      --port 5432 \\
      --command 'select 1 as One;'\\
      postgres" \
  "PGPASSWORD='$postgres_password'"

p \
'Although it is possible to connect to the container by its IP address, it is
also possible to connect to the Postgres on localhost via forwarded port 5001'
sh \
"psql --user postgres \\
      --host localhost \\
      --port 5001 \\
      --command 'select 2 as Two;'\\
      postgres" \
  "PGPASSWORD='$postgres_password'"

h3 'Another example - Redis'

# sh \
snapshot 'redis_container' \
"docker run --publish 6001:6379 \\
            --name $redis_container \\
            --detach \\
        redis:alpine"

p 'Connect to it.'
sh 'redis-cli -p 6001 ping "Hello World!"'

h2 'build image for your app'
p 'Prepare a demo Rails project'

project_dir='demo_rails'

# sh \
snapshot 'clone_repo' \
"git clone https://gitlab.com/mariusz-kowalski/rails-sidekiq.git $project_dir"

cd "$project_dir"

silent_run "git pull"

p 'create *Dockerfile* file in root of the project'
file 'Dockerfile' 'Dockerfile'

image_name='demo-rails'
app_container='demo-rails-app'

p 'To build this image, run `docker build` command:'
snapshot 'app_container_build' "docker build --tag=$image_name ."

p 'To see list of images:'
sh 'docker image list'

p 'Create container from this image'
sh \
"docker create --name $app_container \\
               --network host \\
               --env-file .env-docker \\
        $image_name"

p \
'Option `--network host` configures the container that way it uses hosts network.
All services run in the container all available on hosts localhost. In this case
container serves a web page on port 3000 which is available as it would be run
on local machine - (localhost:3000)[http://localhost:3000].'

p \
'Option `--env-file` will add environment variables in the container from given
file, you may need to adjust it.'

edit_file '.env-docker' \
          'POSTGRES_PASSWORD=.*'
          "POSTGRES_PASSWORD=$postgres_password"

p 'To run a container:'
sh "docker start $app_container"

p 'To list containers'
sh 'docker ps --all'
p '`--all`, `-a` - prints also stopped containers.'

p 'To see logs from the container:'
sh "docker logs $app_container"
p 'Option `--follow` (`-f`) will prints logs in real time.'

p \
'Command `docker run` is a combination of `create` and `start` commands.
Equivalent of the above tho commands would be:'
code "docker run --name $app_container --publish 3000:3000 $image_name"

p \
'Command `run` is especially useful for creating one time use containers, with
different command to run - just add the command at the end with options - it
will override the default `CMD` line from Dockerfile'
sh "docker run --rm $image_name bundle exec rails --version"
p \
'This command will create a new container, with some random name, it will
execute `bundle exec rails --versions` instead of the one defined in the
Dockerfile. After the command ends, the container will stop itself and delete
itself (option `--rm`)'

p 'To run a shell in one time container, use options `--interactive` and `--tty`
(short version `-it`):'
code "docker run --rm --interactive --tty $image_name sh" 'sh'
p 'This should run into interactive shell.'

h2 'Development flow of the application with the container'



teardown
