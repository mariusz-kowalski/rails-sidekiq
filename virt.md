# Virt

dependencies:

* libvirt
* uvtool (replace it with something common)

> switch multipass driver to libvirt

https://help.ubuntu.com/lts/serverguide/cloud-images-and-uvtool.html

## create base machine

Create base virtual machine 18.04lts bionic

```
sudo uvt-kvm create --ssh-public-key-file ~/.ssh/id_rsa.pub \
    docker-intro release=bionic
```

Connect to the machine

```
ssh ubuntu@$(sudo uvt-kvm ip docker-intro)
```

Create initial snapshot

```
sudo virsh snapshot-create-as docker-intro --name initial
```

Revert to initial snapshot

```
sudo virsh snapshot-revert docker-intro --snapshotname initial
```
