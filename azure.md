## cli

> install azure-cli in your OS

login to azure service

```sh
az login
```

find list of available locations

```sh
az account list-locations
```

create a group named 'group0'

```sh
az group create --location northeurope --name group0
```

finally, create docker registry named 'railssidekiqreg'

> can not use '-', '.' nor '_' char to make readable names, thanks MS!

```sh
az acr create --name railssidekiqreg \
              --resource-group group0 \
              --sku standard \
              --admin-enabled true
```

building a Docker image on the Container Registry

```sh
az acr build --file Dockerfile \
             --registry railssidekiqreg \
             --image railssidekiq0 .
```

## getting image from acr

First, you need to login to the Azure registry with `az` command

```sh
az acr login --name railssidekiqreg
```

then pull the image with `docker` command

```sh
docker pull railssidekiqreg.azurecr.io/webimage:latest
```

URI of the image you can find on the Azure panel (All services > Container registers > <registry_name> - Repositories > <image_name>).

Now, we have image locally.

```sh
docker images
```

To run container with this image:

```sh
docker run --name=app_x d7ca307d30a1
```

Find id of the image on list from previous command or use image name. `run` command will in fact create a container named 'app_x' and start it. So, if you want to stop it use `docker stop`, then `docker start` to start it again. Or `docker rm` if you want to remove the container (not the image).

If your container is running, you can run a command in it with 

```sh
docker exec -it app_x /bin/sh
```

`-it` parameter allows you to run command in interactive shell.
